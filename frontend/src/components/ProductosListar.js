import axios from 'axios'
import { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
//const URI = 'https://backend-ticketplus.herokuapp.com/'
const URI = 'http://localhost:4200/'
const CompProductosListar = () => {
    const [productos, setProductos] = useState([])
    useEffect(() => {
        getProductos()
    }, [])
    //METODO PARA LISTAR PRODUCTOS
    const getProductos = async () => {
        const res = await axios.get(`${URI}buscarall/`)
        setProductos(res.data.message)
    }
    //METODO PARA ELIMINAR PRODUCTO
    const deleteProductos = async (id) => {
        await axios.delete(`${URI}producto/${id}`)
        getProductos()
    }
    return (
        <div className='container-lg'>
            <h1>TICKETS GENERADOS</h1>
            <div className='row'>
                <div className='col'>
                    <Link to={'/crear'}className='btn btn-primary mt-2 ml-2 mb-1'><i className="fa-solid fa-plus"></i></Link>
                    <table className='table'>
                        <thead className='table-primary'>
                            <tr>
                                <th>Cliente</th>
                                <th>Precio Articulo</th>
                                <th>Cantidad</th>
                                <th>Detalle de la Reclamacion</th>
                                <th>Agente que recibe</th>
                                <th>acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                            productos.map(productos => (
                                <tr key={productos._id}>
                                    <td>{productos.nombre}</td>
                                    <td>{productos.precio}</td>
                                    <td>{productos.cantidad}</td>
                                    <td>{productos.detalle}</td>
                                    <td>{productos.agente}</td>
                                    <td>
                                        <Link to={`/edit/${productos._id}`}className="fa-solid fa-pen-to-square" style={{color:'white'}}></Link>
                                        
                                        &nbsp;&nbsp;&nbsp;
                                        <Link onClick={()=>deleteProductos(productos._id)} className="fa-solid fa-trash-can" style={{color:'white'}}></Link>
                                    </td>
                                </tr>
                            ))}

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}
export default CompProductosListar
