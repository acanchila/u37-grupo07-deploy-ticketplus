import axios from 'axios'
import {useEffect,useState} from 'react'
import {useNavigate, useParams} from 'react-router-dom'

const URI = 'http://localhost:4200/'
//const URI = 'https://backend-ticketplus.herokuapp.com/'

const CompProductosEditar = ()=>{

    const [nombre,setNombre]=useState('')
    const [precio, setPrecio]=useState('')
    const [cantidad, setCantidad]=useState('')
    const [detalle, setDetalle]=useState('')
    const [agente, setAgente]=useState('')

    const navigate = useNavigate()

    const {id} = useParams()

    const update = async(e)=>{
        e.preventDefault()
        await axios.put(`${URI}producto/${id}`,{
            nombre:nombre, 
            precio:precio, 
            cantidad:cantidad,
            detalle:detalle,
            agente:agente
        })
        navigate('/')
    }
    useEffect(()=>{
        getProductosbyId()
    },[])

    const getProductosbyId =async () => {
        const res = await axios.get(`${URI}buscar/${id}`)
        setNombre(res.data.message.nombre)
        setPrecio(res.data.message.precio)
        setCantidad(res.data.message.cantidad)
        setDetalle(res.data.message.detalle)
        setAgente(res.data.message.agente)
        
    }
    return(
        <div className='container-lg'>
        <h1>MODIFICAR TICKET</h1>
        <form onSubmit={update}>
           <label className='form-label'>Cliente:</label>
           <input
            value={nombre}
            onChange={(e)=>setNombre(e.target.value)}
            type="text"
            className='form-control'
           /> 
    
           <label className='form-label'>Precio Articulo:</label>
           <input
            value={precio}
            onChange={(e)=>setPrecio(e.target.value)}
            type="text"
            className='form-control'
           /> 
           <label className='form-label'>Cantidad:</label>
           <input
            value={cantidad}
            onChange={(e)=>setCantidad(e.target.value)}
            type="text"
            className='form-control'
           /> 
          <label className='form-label'>Detalle de la reclamacion:</label>
           <input
            value={detalle}
            onChange={(e)=>setDetalle(e.target.value)}
            type="text"
            className='form-control'
           /> 
          <label className='form-label'>Agente que recibe la reclamacion:</label>
           <input
            value={agente}
            onChange={(e)=>setAgente(e.target.value)}
            type="text"
            className='form-control'
           /> 
          <button type='submit' className='btn btn-primary mt-2 ml-2'>Modificar</button>
        </form>
      </div>
    )
    }
    
    export default CompProductosEditar

    
    




