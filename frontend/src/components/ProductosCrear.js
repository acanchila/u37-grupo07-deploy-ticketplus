import axios from 'axios'
import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
const URI = 'http://localhost:4200/crear'
//const URI = 'https://backend-ticketplus.herokuapp.com/crear'
const CompProductosCrear = () => {
  const [nombre, setNombre] = useState('')
  const [precio, setPrecio] = useState('')
  const [cantidad, setCantidad] = useState('')
  const [detalle, setDetalle] = useState('')
  const [agente, setAgente] = useState('')
  const navigate = useNavigate()
  const create = async (e) => {
    e.preventDefault()
    await axios.post(URI, { nombre: nombre, precio: precio, cantidad: cantidad,detalle:detalle,agente:agente })
    navigate('/')
  }
  return (
    <div className='container-lg'>
      <h1>NUEVO TICKET</h1>
      <form onSubmit={create}>
        <label className='form-label'>Cliente:</label>
        <input  
          value={nombre}
          onChange={(e) => setNombre(e.target.value)}
          type="text"
          className='form-control'
        />

        <label className='form-label'>Precio Articulo:</label>
        <input  
          value={precio}
          onChange={(e) => setPrecio(e.target.value)}
          type="text"
          className='form-control'
        />
        <label className='form-label'>Cantidad:</label>
        <input 
          value={cantidad}
          onChange={(e) => setCantidad(e.target.value)}
          type="text"
          className='form-control'
        />

        <label className='form-label'>Detalle de la Reclamacion:</label>
        <input 
          value={detalle}
          onChange={(e) => setDetalle(e.target.value)}
          type="text"
          className='form-control'
        />
       <label className='form-label'>Agente que recibe la reclamación:</label>
        <input 
          value={agente}
          onChange={(e) => setAgente(e.target.value)}
          type="text"
          className='form-control'
        />
        <button type='submit' className='btn btn-primary mt-2 ml-2'>Crear</button>
      </form>
    </div>
  )
}
export default CompProductosCrear