import React from 'react';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import './App.css';

//import Navigator from './components/Navigator'
import CompProductosCrear from './components/ProductosCrear';
import CompProductosListar from './components/ProductosListar';
import CompProductosEditar from './components/ProductosEdit';
import NavBarProductos from './layouts/Navbar';

function App() {
return (
  <Router>
   <NavBarProductos />
        <Routes>
            <Route path='/' element={<CompProductosListar/>} />
            <Route path='/crear' element={<CompProductosCrear/>}/>
            <Route path='/edit/:id' element={<CompProductosEditar/>}/>
        </Routes>  
    </Router>
  );
}

export default App;
