var express = require("express");
var app=express();

//importamos el control de cors
const cors = require('cors');
app.use(cors());


var bodyParser = require("body-parser");
//var methodOverride = require("method.override");
var mongoose = require("mongoose");

app.use(express.json());
app.use(express.urlencoded({
    extended:true
}));

app.use((req, res, next)=>{
    res.header('Access-Control-Allow-origin','*');
    res.header('Access-Control-Allow-Headres','Authorization, X-API-KEY, Origin, Xrequested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    
    res.header('Access-Control-Allow-Request-Method','GET,POST,PUT,DELET,OPTIONS');

    res.header('Allow','GET,POST,PUT,DELET,OPTIONS');

    next();
});

app.use(require('./routers/routers'));

module.exports = app;
