# TicketPlus-U37-Grupo06
### INTEGRANTES:
Antonio Jose Canchila Petro - 2224599
## Proyecto
Ticket Plus -Software de tickets.

###### COMAND IMPORT MODULES 
npm init // SOLO PARA INICIAR UN PROYECTO
npm install express --save
npm install bcrypt-nodejs --save
npm install body-parser --save
npm install connect-multiparty --save
npm install jwt-simple --save
npm install moment --save
npm install mongoose --save
npm install mongoose-pagination --save
npm install nodemon --save-dev

## CREACION DEL PROYECTO by Antonio J. Canchila Petro
Se crea el proyecto y se deja probado con la base de datos de prueba. 
el backend corre de forma correcta y se conecta a la base de datos de forma correcta
se hacen pruebas desde postman y todo esta funcional.

## CRUD TERMINADO TABLA Tickets By Antonio J. Canchila Petro
Se realiza el crud completo de la tabla Tickets y se deja funcionando

## CRUD TERMINADO TABLA USUARIO By Milena Diaz
Se realiza el crud completo de la tabla usuario y se deja funcionando

## ENTREGA SPRINT 2 by Antonio J. Canchila Petro y Milena Diaz
Se entrega el backend funcionando correctamente con las evidencias de los endpoints.

## IMPLEMENTACIÓN DEL FRONTEND by Antonio J. Canchila Petro y Milena Diaz
Se implemanta el front end a los modulos realizados

## ENTREGA SPRINT 3 by Antonio J. Canchila Petro y Milena Diaz
Se entrega en front end aplicado al modulo Tickets




