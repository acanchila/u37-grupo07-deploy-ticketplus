const {Router} = require('express');
const router = Router();
var controllerProducto = require('../src/controllers/controllerProducto');
router.post('/crear',controllerProducto.saveProducto);
router.get('/buscar/:id',controllerProducto.buscarData);
router.get('/buscarall/:id?', controllerProducto.listarAllData);
router.put('/producto/:id',controllerProducto.updateProducto);
router.delete('/producto/:id',controllerProducto.deleteProducto);

var controllerUsuario = require('../src/controllers/controllerUsuario');
router.post('/crearusuario',controllerUsuario.saveUsuario);
router.get('/buscarusuario/:id',controllerUsuario.buscarUsuario);
router.get('/buscarallusuario/:id?', controllerUsuario.listarAllUsuario);
router.put('/usuario/:id',controllerUsuario.updateUsuario);
router.delete('/usuario/:id',controllerUsuario.deleteUsuario);
module.exports = router;
