const Producto = require("../models/producto");

function prueba (req,res){
    res.status(200).send({
        message:'Probando controlador prueba mi dulce online'
    })
}

function saveProducto(req,res){
    var myProducto = new Producto(req.body);
    myProducto.save((err,result)=>{
        res.status(200).send({message:result});
     });

}

function buscarData(req, res){
    var idProducto =  req.params.id;

    Producto.findById(idProducto).exec((err,result)=>{
        if(err){
            res.status(500).send({message: 'Error al momento de procesar en el backend'})
        }else{
            if(!result){
                res.status(400).send({message:'Se ha ingresado valores incorrectos y no se puede procesar'})
            }else{
                res.status(200).send({message:result});
            }
        }
    })
}

function listarAllData(req,res){
    var idProducto =  req.params.id;
    if(!idProducto){
var result = Producto.find({}).sort('nombre');
    }else{
        var result = Producto.find({_id:idProducto}).sort('nombre');
    }
    result.exec(function(err,result){
        if(err){
            res.status(500).send({message: 'Error al momento de procesar en el backend'})
        }else{
            if(!result){
                res.status(400).send({message:'Se ha ingresado valores incorrectos y no se puede procesar'})
            }else{
                res.status(200).send({message:result});
            }
        }
    })
}

function updateProducto(req , res){
    var idProducto = req.params.id;
    Producto.findOneAndUpdate({_id:idProducto},req.body,{new:true},function(err,Producto){
        if(err)
            res.send(err)
        res.json(Producto)
    });
}

function deleteProducto(req,res){
    var idProducto = req.params.id;
    Producto.findByIdAndDelete(idProducto,function(err,Producto){
        if(err){
            return res.json(500,{
                message:'No se ha encontrado el producto a Eliminar'
            })
        }
        return res.json(Producto);
    })
}
module.exports = {

    prueba,
    saveProducto,
    listarAllData,
    updateProducto,
    deleteProducto,
    buscarData

}
