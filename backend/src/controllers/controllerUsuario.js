const Usuario = require("../models/usuario"); 
function saveUsuario(req,res){
    var myUsuario = new Usuario(req.body);
    myUsuario.save((err,result)=>{
        res.status(200).send({message:result});
         });
}
function buscarUsuario(req, res){
    var idUsuario =  req.params.id;

    Usuario.findById(idUsuario).exec((err,result)=>{
        if(err){
            res.status(500).send({message: 'Error en el backend'})
        }else{
            if(!result){
                res.status(400).send({message:'Datos incorrectos'})
            }else{
                res.status(200).send({message:result});
            }
        }
    })
}
function listarAllUsuario(req,res){
    var idUsuario =  req.params.id;
    if(!idUsuario){
var result = Usuario.find({}).sort('nombre');
    }else{
        var result = Usuario.find({_id:idUsuario}).sort('nombre');
    }
    result.exec(function(err,result){
        if(err){
            res.status(500).send({message: 'Error en el backend'})
        }else{
            if(!result){
                res.status(400).send({message:'Error en los datos'})
            }else{
                res.status(200).send({message:result});
            }
        }
    })
}
function updateUsuario(req , res){
    var idUsuario = req.params.id;
    Usuario.findOneAndUpdate({_id:idUsuario},req.body,{new:true},function(err,Usuario){
        if(err)
            res.send(err)
        res.json(Usuario)
    });
}
function deleteUsuario(req,res){
    var idUsuario = req.params.id;
    Usuario.findByIdAndDelete(idUsuario,function(err,Usuario){
        if(err){
            return res.json(500,{
                message:'No se ha encontrado Usario a Eliminar'
            })
        }
        return res.json(Usuario);
    })
}
module.exports = {
    saveUsuario,
    listarAllUsuario,
    deleteUsuario,
    updateUsuario,
    buscarUsuario
}