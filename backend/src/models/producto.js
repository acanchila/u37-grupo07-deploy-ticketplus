var mongoose = require('mongoose');

var schema = mongoose.Schema;

var productoSchema = schema({
    nombre:String,
    precio: Number,
    cantidad:String,
    detalle:String,
    agente:String
    
});

const Producto = mongoose.model('tbc_producto', productoSchema);
module.exports = Producto;
