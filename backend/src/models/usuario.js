var mongoose = require('mongoose');

var schema = mongoose.Schema;

var usuarioSchema = schema({
    nombre:String,
    apellido: String,
    numero_documento: Number,
    celular: Number,
    dirrección_envio: String
    
});

const Usuario = mongoose.model('tbl_usuario', usuarioSchema);
module.exports = Usuario;